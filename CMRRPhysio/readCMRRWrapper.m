function readCMRRWrapper(varargin)

% set filename for testing and debugging
%fn='/home/shared/HCP/taskfmri/phase2/fmri/WORK/GREG_work/PHYSIO_PROCESSING/HCP_DICOM_FORMAT/LIFESPAN_scans/HCA7577595_V1_A/SCANS/11/DICOM/1.3.12.2.1107.5.2.43.166038.30000017083012445187200000010-11-1-1u7d2es.dcm'; % WUSM
%fn='/home/shared/HCP/taskfmri/phase2/fmri/WORK/GREG_work/PHYSIO_PROCESSING/HCP_DICOM_FORMAT/LIFESPAN_scans/HCD2936875_V1_A/SCANS/11/DICOM/1.3.12.2.1107.5.2.43.67056.30000017082915182357500000145-11-1-smrool.dcm'; % Harvard
%addpath('/home/shared/HCP/taskfmri/phase2/fmri/WORK/GREG_work/PHYSIO_PROCESSING/HCP_DICOM_FORMAT/CMRR_code_download_2017-09-15')

% check input arguments
if (nargin < 1 || nargin > 2)
    error('Invalid number of inputs.');
end

if (nargin == 1)
    fn = varargin{1};
    VolSkip = 10; % Default value, used for LifeSpan
elseif (nargin == 2)
    fn = varargin{1};
    VolSkip = str2num(varargin{2});
end
fprintf('NOTE:  %d volumes will be skipped in this run.\n',VolSkip);

[filepath,name,ext]=fileparts(fn);

if(~isdeployed)
      cd(filepath);
end

physio=readCMRRPhysio(strcat(name,ext));

% Make vectors of zeroes just in case vectors are empty in physio struct
[PULS,RESP,ACQ,EXT]=deal(zeros(length(physio.ACQ),1,'uint16'));

if (isfield(physio,'EXT'))
    EXT=physio.EXT;
end
if (isfield(physio,'EXT2'))
    EXT=physio.EXT2;
end
if (isfield(physio,'EXT3'))
    EXT=physio.EXT3;
end
if (isfield(physio,'PULS'))
    PULS=physio.PULS;
end
if (isfield(physio,'RESP'))
    RESP=physio.RESP;
end
if (isfield(physio,'ACQ'))
    ACQ=physio.ACQ;
end

% Find all transitions from 0->1 (diff of [0 1 0] would be [0 1 -1])
ACQIndex = find([0;diff(ACQ)] == 1);
% This is the number of repeat acquisitions per volume
MBStep = length(ACQIndex)./size(physio.SliceMap,2);
fprintf('\nMBStep value:  %d',MBStep);
if(mod(MBStep,1) ~= 0)
    error('Number of counts is not a multiple of number of volumes');
end
% Create an empty vector equal to length of ACQ and fill with appropriate
% ones
TRIG = zeros(size(ACQ));
TRIG(ACQIndex(1+(VolSkip*MBStep):MBStep:end)) = 1; %#ok<BDSCI>

% make table of physio outputs ordered ACQ,EXT,RESP,PULS
% (since EXT is the most likely empty vector)
% physio.ACQ, physio.EXT, physio.RESP, physio.PULS
if(var([length(ACQ) length(EXT) length(RESP) length(PULS)])~=0)
	error('ERROR: Length of physio vectors are unequal:\nlength of ACQ = %d\nlength of EXT = %d\nlength of RESP = %d\nlength of PULS = %d\n', length(ACQ), length(EXT), length(RESP), length(PULS))
else
	T = table(TRIG,ACQ,EXT,RESP,PULS);
	outname=strcat('Physio_combined_',physio.UUID{1,1},'.csv');
	writetable(T,outname);  % Default delimiter is csv
end


