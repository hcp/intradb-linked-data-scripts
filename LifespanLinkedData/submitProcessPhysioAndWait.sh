OPTIONS=$@
JOBNAME=procPhysioAndEV
pushd /data/intradb/logs/sge
echo "Submitting $JOBNAME for processing"
JOBID=`qsub -sync yes -N $JOBNAME -cwd /nrgpackages/tools.release/intradb/LifespanLinkedData/processPhysio.sh $OPTIONS | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}' | head -1`
RC=$?
printf "\nJOBID=$JOBID\n"
OUTFILE="${JOBNAME}.o${JOBID}"
ERRFILE="${JOBNAME}.e${JOBID}"
if [ -f "$ERRFILE" ] && [[ `cat $ERRFILE | wc -l` -gt 0 ]] ; then
	printf "\nSTDERR:\n\n";
	cat $ERRFILE
fi
if [ -f "$OUTFILE" ] && [[ `cat $OUTFILE | wc -l` -gt 0 ]] ; then
	printf "\nSTDOUT:\n\n";
	cat $OUTFILE
fi
if [ "$RC" -eq 0 ] ; then
	printf "\nSGE Job finished successfully (JOBID=$JOBID)\n\n"
else 
	printf "\nSGE processing not successful (JOBID=$JOBID, RC=$RC)\n\n"
fi
popd 
