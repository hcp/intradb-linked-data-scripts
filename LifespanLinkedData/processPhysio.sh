#!/bin/bash

#INITIALIZE PYTHON
VENV_PYTHON_HOME=/nrgpackages/tools.release/intradb/python/venv
source ${VENV_PYTHON_HOME}/bin/activate
PATH=${VENV_PYTHON_HOME}/bin:${PATH}
export PATH

CMRR_PHYSIO="/nrgpackages/tools.release/intradb/CMRRPhysio"
DCM_DUMP="/nrgpackages/tools.release/dcmtk-3.6.1_20111208-install/bin/dcmdump"
TMPDIR=`mktemp -d --suffix=LsLinked`
PHYSIO_LOG="launch_matlab_physio.log"
LOG_FILENAME="processPhysio.log"
LOG_FILE="$TMPDIR/$LOG_FILENAME"
KEEP_TEMP_DIR=false
SECURE=true
VOL_SKIP_INFO=""

while true; do 
    case "$1" in
      --help | -h | -\?)
	printf "\nprocessPhysio.sh [options]\n\n"
	printf "   Options\n\n"
	printf "      -H, --host             <server>\n"
	printf "      -U, --user             <user>\n"
	printf "      -P, --pw               <password>\n"
	printf "      -S, --secure	   \n"
	printf "      -p, --project          <connectomedb project>\n"
	printf "      -s, --subject          <subject label>\n"
	printf "      -e, --exp              <experiment label>\n"
	printf "      --keep-temp-dir                                     (If applicable)         \n"
	printf "      --volSkipInfo          (OPTIONAL) - Series description based regular expressions to control number of skipped volumes \n\n"
	exit 0
	;;
      --host | -H)
        HOST=$2
	shift
	shift
        ;;
      --user | -U)
        USR=$2
	shift
	shift
        ;;
      --pw | -P)
        PW=$2
	shift
	shift
        ;;
      --project | -p)
        PROJ=$2
	shift
	shift
        ;;
      --subject | -s)
        SUBJ=$2
	shift 
	shift 
        ;;
      --exp | -e)
        EXP=$2
	shift 
	shift 
        ;;
      --insecure | -I)
        SECURE=false
	shift 
        ;;
      --keep-temp-dir)
        KEEP_TEMP_DIR=true
	shift 
        ;;
      --volSkipInfo)
        VOL_SKIP_INFO=$2
	shift 
        ;;
      -*)
	echo "Invalid parameter ($1)"
	exit 1
        ;;
      *)
	break 
        ;;
    esac
done

if [[ $HOST == http[s:][/:]* ]] ; then
	HOST_URI="${HOST}"
else
	if $SECURE ; then
		HOST_URI="https://${HOST}"
	else
		HOST_URI="http://${HOST}:8080"
	fi
fi
JSESSIONID=`curl -s -k -u $USR:$PW $HOST_URI/data/JSESSIONID; export JSESSIONID`
#PROJ="CCF_HCA_ITK"
#SUBJ="HCA6002236"
#EXP="HCA6002236_V1_A"
ARCHIVE="/data/intradb/archive/$PROJ/arc001/$EXP"

printf "Running Physio file generation for:\n\n"  | tee -a $LOG_FILE
printf " HOST_URI=$HOST_URI\n" | tee -a $LOG_FILE
printf " PROJ=$PROJ\n" | tee -a $LOG_FILE
printf " SUBJ=$SUBJ\n" | tee -a $LOG_FILE
printf " EXP=$EXP\n\n" | tee -a $LOG_FILE

SESSION_STATUS=`curl -s -I -o $TMPDIR/${EXP}_GET.txt -w "%{http_code}" -k --cookie JSESSIONID=$JSESSIONID $HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP?format=xml`

echo "SESSION_STATUS=$SESSION_STATUS" | tee -a $LOG_FILE

if [[ $SESSION_STATUS -gt 205 ]] || [[ $SESSION_STATUS -lt 200 ]] ; then
	echo "ERROR:  Could not access the session (HTTP_STATUS=$SESSION_STATUS)!" | tee -a $LOG_FILE
	exit 9;
fi

if [[ ! -d "$ARCHIVE" ]] ; then
	echo "ERROR:  Archive directory does not exist!" | tee -a $LOG_FILE
	exit 9;
fi

#echo "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans?columns=xnat_imagescan_id,ID,type,series_description,URI&format=csv" ;

## 
## PHYSIO PROCESSING
## 

printf "\nBEGIN PROCESSING PHYSIO DATA\n" | tee -a $LOG_FILE

## MRH: NOTE!!!  Excluding dMRI physio generation for now.   
for SCANINFO in `curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans?columns=xnat_imagescan_id,ID,type,series_description,URI&format=csv" | grep "Physio" | grep -v "dMRI"`; do
	SCAN=`echo "$SCANINFO" | cut -d, -f2`;
	SERIES_DESC=`echo "$SCANINFO" | cut -d, -f4`;

	VOL_SKIP_VAL="10"
	export IFS=","
	for GRP in $VOL_SKIP_INFO; do
		echo "$GRP"
		IFS=":" read -r -a SKIPINFO <<< "$GRP"
		REGEX="${SKIPINFO[0]}";
		VOL_SKIP="${SKIPINFO[1]}";
		if [[ "$SERIES_DESC" =~ $REGEX ]]; then
			VOL_SKIP_VAL=$VOL_SKIP
			continue
		fi
		export IFS=","
	done

	printf "\nProcessing Physio scan: $SCAN ($SERIES_DESC)\n" | tee -a $LOG_FILE
	printf "\nNumber of volumes to be skipped for this scan:  $VOL_SKIP_VAL\n" | tee -a $LOG_FILE
	DICOMDIR="$ARCHIVE/SCANS/$SCAN/DICOM"

	if [[ ! -d "$DICOMDIR" ]] ; then
		echo "ERROR:  Expected archive directory does not exist ($DICOMDIR)." | tee -a $LOG_FILE
		continue
	fi

	DICOM_TEMP_DIR="$TMPDIR/SCANS/$SCAN/DICOM"
	mkdir -p $DICOM_TEMP_DIR

	DICOMFILES=`find $DICOMDIR -type f | egrep -v "\.xml"`
	printf "\nChecking for dicom files in $DICOMDIR:\n" | tee -a $LOG_FILE
	for DICOMFILE in `echo "$DICOMFILES"`; do
		if `$DCM_DUMP $DICOMFILE 2>/dev/null | grep -q PHYSIO && true || false` ; then
			printf "\nFound Physio DICOM file: $DICOMFILE\n" | tee -a $LOG_FILE
			ln -s $DICOMFILE $DICOM_TEMP_DIR/physio${SCAN}.dcm 
		fi;
	done

	pushd "$DICOM_TEMP_DIR"
	for PROCFILE in `find . -type l`; do
		echo "addpath $CMRR_PHYSIO; readCMRRWrapper ('$PROCFILE', '$VOL_SKIP_VAL')" | matlab -nojvm -nodisplay -nosplash >> $PHYSIO_LOG 2>&1 
		PHYSIOFILE=`ls -1 *.csv` 
		if [[ `echo "${#PHYSIOFILE}"` -lt 1 ]] ; then
			printf "\nERROR:  Physio CSV file could not be generated.  PHYSIO LOG FILE OUTPUT:  \n" | tee -a $LOG_FILE
			cat $PHYSIO_LOG | tee -a $LOG_FILE
			printf "\n\nEND OF LOG\n\n" | tee -a $LOG_FILE
			continue
		fi
		UUID=`echo "$PHYSIOFILE" | sed -e "s/^.*_//" -e "s/\..*$//"`
		echo "$PHYSIOFILE"
		if [[ ${#UUID} -gt 10 ]] ; then
				printf "\nPhysio CSV created.  FILE=$PHYSIOFILE, UUID=$UUID\n" | tee -a $LOG_FILE
			TOSCAN=`curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans?columns=xnat_imagescan_id,ID,type,series_description,xnat:mrscandata/fileNameUUID,URI&format=csv" | grep ",${UUID}," | grep -v "SBRef" | cut -d, -f3`
			echo "$TOSCAN"
			if [[ `echo "$TOSCAN" | wc -l` -eq 1 ]] && [[ `echo "${#TOSCAN}"` -gt 0 ]]; then
				printf "\nUploading to scan $TOSCAN\n\n" | tee -a $LOG_FILE
				curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans/$TOSCAN/resources/LINKED_DATA" -X POST &>/dev/null
				curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans/$TOSCAN/resources/LINKED_DATA/files/PHYSIO/$PHYSIOFILE?overwrite=true" -X POST -F "file=@$PHYSIOFILE" -i | tee -a $LOG_FILE
			else
				printf "\nERROR:  Wrong number of scans found:\n\n$TOSCAN\n\n" | tee -a $LOG_FILE
			fi
		fi 
	done
	popd

done

printf "\nPHYSIO PROCESSING COMPLETE\n\n" | tee -a $LOG_FILE

## 
## UPLOAD LOG FILE
## 

if [[ -d "$TMPDIR" ]] ; then

	printf "\nUploading Log File:  $LOG_FILE\n" | tee -a $LOG_FILE

	curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/resources/LINKED_DATA" -X POST &>/dev/null
	curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/resources/LINKED_DATA/files/LOGS/$LOG_FILENAME?overwrite=true" -X POST -F "file=@$LOG_FILE" -i | tee -a $LOG_FILE

	if ! $KEEP_TEMP_DIR && [[ $TMPDIR =~ ^.*LsLinked$ ]] ; then
		printf "\nCleaning up temp directory:  $TMPDIR\n" | tee -a $LOG_FILE
		rm -r $TMPDIR
	fi

fi

