#!/bin/bash

#INITIALIZE PYTHON
VENV_PYTHON_HOME=/nrgpackages/tools.release/intradb/python/venv
source ${VENV_PYTHON_HOME}/bin/activate
PATH=${VENV_PYTHON_HOME}/bin:${PATH}
export PATH

CMRR_PHYSIO="/nrgpackages/tools.release/intradb/CMRRPhysio"
LIFESPAN_EV="/nrgpackages/tools.release/intradb/LifespanEV"
DCM_DUMP="/nrgpackages/tools.release/dcmtk-3.6.1_20111208-install/bin/dcmdump"
TMPDIR=`mktemp -d --suffix=LsLinked`
PHYSIO_LOG="launch_matlab_physio.log"
LOG_FILENAME="processPhysioAndEV.log"
LOG_FILE="$TMPDIR/$LOG_FILENAME"
KEEP_TEMP_DIR=false
SECURE=true

while true; do 
    case "$1" in
      --help | -h | -\?)
	printf "\nprocessPhysioAndEV.sh [options]\n\n"
	printf "   Options\n\n"
	printf "      -H, --host             <server>\n"
	printf "      -U, --user             <user>\n"
	printf "      -P, --pw               <password>\n"
	printf "      -S, --secure	   \n"
	printf "      -p, --project          <connectomedb project>\n"
	printf "      -s, --subject          <subject label>\n"
	printf "      -e, --exp              <experiment label>\n"
	printf "      --keep-temp-dir                                     (If applicable)         \n\n"
	exit 0
	;;
      --host | -H)
        HOST=$2
	shift
	shift
        ;;
      --user | -U)
        USR=$2
	shift
	shift
        ;;
      --pw | -P)
        PW=$2
	shift
	shift
        ;;
      --project | -p)
        PROJ=$2
	shift
	shift
        ;;
      --subject | -s)
        SUBJ=$2
	shift 
	shift 
        ;;
      --exp | -e)
        EXP=$2
	shift 
	shift 
        ;;
      --insecure | -I)
        SECURE=false
	shift 
        ;;
      --keep-temp-dir)
        KEEP_TEMP_DIR=true
	shift 
        ;;
      -*)
	echo "Invalid parameter ($1)"
	exit 1
        ;;
      *)
	break 
        ;;
    esac
done

if [[ $HOST == http[s:][/:]* ]] ; then
	HOST_URI="${HOST}"
else
	if $SECURE ; then
		HOST_URI="https://${HOST}"
	else
		HOST_URI="http://${HOST}:8080"
	fi
fi
JSESSIONID=`curl -s -k -u $USR:$PW $HOST_URI/data/JSESSIONID; export JSESSIONID`
#PROJ="CCF_HCA_ITK"
#SUBJ="HCA6002236"
#EXP="HCA6002236_V1_A"
ARCHIVE="/data/intradb/archive/$PROJ/arc001/$EXP"

printf "Running EV/Physio file generation for:\n\n"  | tee -a $LOG_FILE
printf " HOST_URI=$HOST_URI\n" | tee -a $LOG_FILE
printf " PROJ=$PROJ\n" | tee -a $LOG_FILE
printf " SUBJ=$SUBJ\n" | tee -a $LOG_FILE
printf " EXP=$EXP\n\n" | tee -a $LOG_FILE

SESSION_STATUS=`curl -s -I -o $TMPDIR/${EXP}_GET.txt -w "%{http_code}" -k --cookie JSESSIONID=$JSESSIONID $HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP?format=xml`

echo "SESSION_STATUS=$SESSION_STATUS" | tee -a $LOG_FILE

if [[ $SESSION_STATUS -gt 205 ]] || [[ $SESSION_STATUS -lt 200 ]] ; then
	echo "ERROR:  Could not access the session (HTTP_STATUS=$SESSION_STATUS)!" | tee -a $LOG_FILE
	exit 9;
fi

if [[ ! -d "$ARCHIVE" ]] ; then
	echo "ERROR:  Archive directory does not exist!" | tee -a $LOG_FILE
	exit 9;
fi

#echo "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans?columns=xnat_imagescan_id,ID,type,series_description,URI&format=csv" ;

## 
## PHYSIO PROCESSING
## 

printf "\nBEGIN PROCESSING PHYSIO DATA\n" | tee -a $LOG_FILE

## MRH: NOTE!!!  Excluding dMRI physio generation for now.   
for SCAN in `curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans?columns=xnat_imagescan_id,ID,type,series_description,URI&format=csv" | grep "Physio" | grep -v "dMRI" | cut -d, -f2`; do

	printf "\nProcessing Physio scan: $SCAN\n" | tee -a $LOG_FILE
	DICOMDIR="$ARCHIVE/SCANS/$SCAN/DICOM"

	if [[ ! -d "$DICOMDIR" ]] ; then
		echo "ERROR:  Expected archive directory does not exist ($DICOMDIR)." | tee -a $LOG_FILE
		continue
	fi

	DICOM_TEMP_DIR="$TMPDIR/SCANS/$SCAN/DICOM"
	mkdir -p $DICOM_TEMP_DIR

	DICOMFILES=`find $DICOMDIR -type f | egrep -v "\.xml"`
	printf "\nChecking for dicom files in $DICOMDIR:\n" | tee -a $LOG_FILE
	for DICOMFILE in `echo "$DICOMFILES"`; do
		if `$DCM_DUMP $DICOMFILE 2>/dev/null | grep -q PHYSIO && true || false` ; then
			printf "\nFound Physio DICOM file: $DICOMFILE\n" | tee -a $LOG_FILE
			ln -s $DICOMFILE $DICOM_TEMP_DIR/physio${SCAN}.dcm 
		fi;
	done

	pushd "$DICOM_TEMP_DIR"
	for PROCFILE in `find . -type l`; do
		echo "addpath $CMRR_PHYSIO; readCMRRWrapper ('$PROCFILE')" | matlab -nojvm -nodisplay -nosplash >> $PHYSIO_LOG 2>&1 
		PHYSIOFILE=`ls -1 *.csv` 
		if [[ `echo "${#PHYSIOFILE}"` -lt 1 ]] ; then
			printf "\nERROR:  Physio CSV file could not be generated.  PHYSIO LOG FILE OUTPUT:  \n" | tee -a $LOG_FILE
			cat $PHYSIO_LOG | tee -a $LOG_FILE
			printf "\n\nEND OF LOG\n\n" | tee -a $LOG_FILE
			continue
		fi
		UUID=`echo "$PHYSIOFILE" | sed -e "s/^.*_//" -e "s/\..*$//"`
		echo "$PHYSIOFILE"
		if [[ ${#UUID} -gt 10 ]] ; then
				printf "\nPhysio CSV created.  FILE=$PHYSIOFILE, UUID=$UUID\n" | tee -a $LOG_FILE
			TOSCAN=`curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans?columns=xnat_imagescan_id,ID,type,series_description,xnat:mrscandata/fileNameUUID,URI&format=csv" | grep ",${UUID}," | grep -v "SBRef" | cut -d, -f3`
			echo "$TOSCAN"
			if [[ `echo "$TOSCAN" | wc -l` -eq 1 ]] && [[ `echo "${#TOSCAN}"` -gt 0 ]]; then
				printf "\nUploading to scan $TOSCAN\n\n" | tee -a $LOG_FILE
				curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans/$TOSCAN/resources/LINKED_DATA" -X POST &>/dev/null
				curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans/$TOSCAN/resources/LINKED_DATA/files/PHYSIO/$PHYSIOFILE?overwrite=true" -X POST -F "file=@$PHYSIOFILE" -i | tee -a $LOG_FILE
			else
				printf "\nERROR:  Wrong number of scans found:\n\n$TOSCAN\n\n" | tee -a $LOG_FILE
			fi
		fi 
	done
	popd

done

printf "\nPHYSIO PROCESSING COMPLETE\n\n" | tee -a $LOG_FILE


## 
## PSYCHOPY PROCESSING
## 

printf "\nBEGIN PROCESSING PSYCHOPY DATA\n" | tee -a $LOG_FILE

#for SCANINFO in `curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans?columns=xnat_imagescan_id,ID,type,series_description,URI&format=csv" | egrep ",zfMRI,|,rfMRI,|,mbPCASLhr,|,mbPCASL," | cut -d, -f2,3,4`; do
for SCANINFO in `curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans?columns=xnat_imagescan_id,ID,type,series_description,URI&format=csv" | egrep ",tfMRI," | cut -d, -f2,3,4`; do

	IFS=',' read -ra SCANARR <<< "$SCANINFO"
	SCAN_ID=${SCANARR[0]}
	SCAN_TYPE=${SCANARR[1]}
	SERIES_DESC=${SCANARR[2]}
	#printf "\n$SCAN_ID  $SCAN_TYPE  $SERIES_DESC\n" | tee -a $LOG_FILE

	PSYCHOPYDIR="$ARCHIVE/SCANS/$SCAN_ID/LINKED_DATA/PSYCHOPY"

	if [[ ! -d "$PSYCHOPYDIR" ]] ; then
		echo "ERROR:  Expected archive directory does not exist ($PSYCHOPYDIR)." | tee -a $LOG_FILE
		continue
	fi

	PSYCHOPY_TEMP_DIR="$TMPDIR/SCANS/$SCAN_ID/LINKED_DATA/PSYCHOPY"
	mkdir -p $PSYCHOPY_TEMP_DIR

	PSYCHOPYFILES=`find $PSYCHOPYDIR -type f | egrep -v "\.xml|_run[0-9]_wide.csv|\/EVs\/"`
	printf "\nChecking for PSYCHOPY files in $PSYCHOPYDIR:\n\n" | tee -a $LOG_FILE
	for PSYCHOPYFILE in `echo "$PSYCHOPYFILES"`; do
		printf "Found PSYCHOPY file: $PSYCHOPYFILE\n" | tee -a $LOG_FILE
		PSYCHOPYFN=`echo "$PSYCHOPYFILE" | sed -e "s/^.*\///"`
		ln -s $PSYCHOPYFILE $PSYCHOPY_TEMP_DIR/$PSYCHOPYFN
	done

	pushd "$PSYCHOPY_TEMP_DIR"

	mkdir ./EVs

	if [ "$PROJ" == "CCF_HCA_ITK" ] ; then

		printf "SERIES_DESC=$SERIES_DESC\n"

		if [[ "$SERIES_DESC" =~ .*CARIT.* ]] ; then
	
			echo "PROCESS CARIT SCAN"

			column_string="trialNum stim corrAns prepotency ISI countdownStartTime countdownLabel shapeStartTime shapeEndTime fixStartTime fixEndTime runEndTime resp trialResp.keys trialResp.rt trialResp.firstKey trialResp.firstRt corrRespMsg corrRespCode corrRespTrialType isiPress.keys isiPress.rt hitCount missCount falseAlarmCount corrRejectCount totalAcc goAcc nogoAcc goFiveBackAvg nogoFiveBackAvg totalFiveBackAvg condFile mriMode frameRate  git-revision nRuns"

			# These inputs are the original files with timestamps in filenames
			# Output files written in $PWD without timestamps
			printf "\nGenerate CARIT Log Files:\n"
			for file in $(ls *run{1,2}*wide.csv); do
				printf "file=$file\n"
				python $LIFESPAN_EV/make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
			done
			cleanInputFile=`ls -1 *CARIT*run[0-9]_wide.csv`
			printf "\nGenerate CARIT EV Files:\n"
			if [ "${#cleanInputFile}" -gt 0 ] ; then
				printf "cleanInputFile=$cleanInputFile\n"
				$LIFESPAN_EV/make_HCA_CARIT_EVs.sh $cleanInputFile ./EVs
			fi

		elif [[ "$SERIES_DESC" =~ .*VISMOTOR.* ]] ; then

			echo "PROCESS VISMOTOR SCAN"

			column_string="trialNum countdownStartTime countdownLabel fixStartTime fixEndTime fixDur blockStartTime blockEndTime targetStartTime targetEndTime side firstKey firstRt correct trialResp.keys trialResp.rt earlyISI.keys lateISI.keys feedback resp respLast noResp correctResponse leftCount rightCount leftCorrCount rightCorrCount leftAcc rightAcc totalAcc totalFiveBackAvg countConsecutive trialsList mriMode runNumber frameRate git-revision run"

			# These inputs are the original files with timestamps in filenames
			# Output files written in $PWD without timestamps
			printf "\nGenerate VISMOTOR Log Files:\n"
			for file in $(ls *run{1,2}*wide.csv); do
				printf "file=$file\n"
				python $LIFESPAN_EV/make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
			done
			cleanInputFile=`ls -1 *VISMOTOR*run[0-9]_wide.csv`
			printf "\nGenerate VISMOTOR EV Files:\n"
			if [ "${#cleanInputFile}" -gt 0 ] ; then
				printf "cleanInputFile=$cleanInputFile\n"
				$LIFESPAN_EV/make_HCA_VISMOTOR_EVs.sh $cleanInputFile ./EVs
			fi



		elif [[ "$SERIES_DESC" =~ .*FACENAME.* ]] ; then
	
			echo "PROCESS FACENAME SCAN"

			column_string="trialNum block stim FaceLabel countdownStartTime countdownLabel cueStartTime cueEndTime faceStartTime faceEndTime gngShapeStartTime gngShapeEndTime fixStartTime fixEndTime runEndTime resp trialResp.keys trialResp.rt corrAns ISI respGNG.keys respGNG.corr respGNG.rt isiPress.keys isiPress.rt runNum mriMode frameRate counterbalance git-revision"

			# These inputs are the original files with timestamps in filenames
			# Output files written in $PWD without timestamps
			printf "\nGenerate FACENAME Log Files:\n"
			for file in $(ls *run{1,2}*wide.csv); do
				printf "file=$file\n"
				python $LIFESPAN_EV/make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
			done
			cleanInputFile=`ls -1 *FACENAME*run[0-9]_wide.csv`
			printf "\nGenerate FACENAME EV Files:\n"
			if [ "${#cleanInputFile}" -gt 0 ] ; then
				printf "cleanInputFile=$cleanInputFile\n"
				$LIFESPAN_EV/make_HCA_FACENAME_EVs.sh $cleanInputFile ./EVs
			fi

		#elif [[ "$SCAN_TYPE" =~ .*rfMRI.* ]] || [[ "$SCAN_TYPE" =~ .*mbPCASL.* ]] ; then
		#
		#	echo "PROCESSING $SERIES_DESC !!!!"
		#
		else 
	
			echo "SCAN_TYPE=$SCAN_TYPE"
			echo "WARNING:  SeriesDescription $SERIES_DESC does not match any descriptions to be processed."

		fi

	elif [ "$PROJ" == "CCF_HCD_ITK" ] ; then

		printf "SERIES_DESC=$SERIES_DESC\n"

		if [[ "$SERIES_DESC" =~ .*CARIT.* ]] ; then
	
			echo "PROCESS CARIT SCAN"

			column_string="trialNum stim corrAns prepotency ISI countdownStartTime countdownLabel shapeStartTime shapeEndTime fixStartTime fixEndTime trialOutcome nogoCondition resp trialResp.keys trialResp.rt trialResp.firstKey trialResp.firstRt corrRespMsg corrRespCode corrRespTrialType isiPress.keys isiPress.rt hitCount missCount falseAlarmCount corrRejectCount totalAcc goAcc nogoAcc goFiveBackAvg nogoFiveBackAvg totalFiveBackAvg prevRewShape condFile mriMode frameRate git-revision nRuns"

			# These inputs are the original files with timestamps in filenames
			# Output files written in $PWD without timestamps
			printf "\nGenerate CARIT Log Files:\n"
			for file in $(ls *run{1,2}*wide.csv); do
				printf "file=$file\n"
				python $LIFESPAN_EV/make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
			done
			cleanInputFile=`ls -1 *CARIT*run[0-9]_wide.csv`
			printf "\nGenerate CARIT EV Files:\n"
			if [ "${#cleanInputFile}" -gt 0 ] ; then
				printf "cleanInputFile=$cleanInputFile\n"
				$LIFESPAN_EV/make_HCD_CARIT_EVs.sh $cleanInputFile ./EVs
			fi

		elif [[ "$SERIES_DESC" =~ .*GUESSING.* ]] ; then

			echo "PROCESS GUESSING SCAN"

			column_string="trialNum countdownStartTime countdownLabel cueStartTime cueEndTime isi1StartTime isi1EndTime ISI1 guessStartTime guessEndTime isi2StartTime isi2EndTime ISI2 feedbackStartTime feedbackEndTime feedbackName valueCondition guessResp.firstKey guessResp.firstRt guessResp.firstGuess guessResp.keys guessResp.rt isiPress1.keys isiPress1.rt isiPress2.keys isiPress2.rt cumulativeNoResp cumulativeReward rewardAmount rewShape mriMode nRuns frameRate git-revision run"

			# These inputs are the original files with timestamps in filenames
			# Output files written in $PWD without timestamps
			printf "\nGenerate GUESSING Log Files:\n"
			for file in $(ls *run{1,2}*wide.csv); do
				printf "file=$file\n"
				python $LIFESPAN_EV/make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
			done
			cleanInputFile=`ls -1 *GUESSING*run[0-9]_wide.csv`
			printf "\nGenerate GUESSING EV Files:\n"
			if [ "${#cleanInputFile}" -gt 0 ] ; then
				printf "cleanInputFile=$cleanInputFile\n"
				$LIFESPAN_EV/make_HCD_GUESSING_EVs.sh $cleanInputFile ./EVs
			fi



		elif [[ "$SERIES_DESC" =~ .*EMOTION.* ]] ; then
	
			echo "PROCESS EMOTION SCAN"

			column_string="trialNum trialCondition countdownStartTime countdownLabel cueStartTime cueEndTime trialStartTime trialEndTime fixStartTime fixEndTime runEndTime stimLeft stimTop stimRight corrAns trialResp.keys trialResp.rt resp msg corrRespMsg corrRespCode countConsecutive mriMode frameRate counterbalance"

			# These inputs are the original files with timestamps in filenames
			# Output files written in $PWD without timestamps
			printf "\nGenerate EMOTION Log Files:\n"
			for file in $(ls *run{1,2}*wide.csv); do
				printf "file=$file\n"
				python $LIFESPAN_EV/make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
			done
			cleanInputFile=`ls -1 *EMOTION*run[0-9]_wide.csv`
			printf "\nGenerate EMOTION EV Files:\n"
			if [ "${#cleanInputFile}" -gt 0 ] ; then
				printf "cleanInputFile=$cleanInputFile\n"
				$LIFESPAN_EV/make_HCD_EMOTION_EVs.sh $cleanInputFile ./EVs
			fi

		#elif [[ "$SCAN_TYPE" =~ ".*rfMRI.*" ]] | [[ "$SCAN_TYPE" =~ ".*mbPCASL.*" ]] ; then
		#
		#	echo "PROCESSING $SERIES_DESC !!!!"
		#	for file in *design* *ts* *EyeCamFPS_Dist* *mp4*; do
		#		copy_psychopy_remove_filename_timestamps.sh $file;
		#	done
		#
		else 
	
			echo "SCAN_TYPE=$SCAN_TYPE"
			echo "WARNING:  SeriesDescription $SERIES_DESC does not match any descriptions to be processed."

		fi


	fi

	pushd ..

	## Used find here for files so we do not post back the symlinked files.
	find PSYCHOPY -type f | xargs -I '{}' zip -r ev.zip {}
	curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans/$SCAN_ID/resources/LINKED_DATA" -X POST &>/dev/null
	curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/scans/$SCAN_ID/resources/LINKED_DATA/files/ev.zip?extract=true&overwrite=true" -X POST -F "file=@ev.zip" -i | tee -a $LOG_FILE

	popd

	popd

done

printf "\nPSYCHOPY PROCESSING COMPLETE\n\n" | tee -a $LOG_FILE

## 
## UPLOAD LOG FILE
## 

if [[ -d "$TMPDIR" ]] ; then

	printf "\nUploading Log File:  $LOG_FILE\n" | tee -a $LOG_FILE

	curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/resources/LINKED_DATA" -X POST &>/dev/null
	curl -s -k --cookie JSESSIONID=$JSESSIONID "$HOST_URI/data/projects/$PROJ/subjects/$SUBJ/experiments/$EXP/resources/LINKED_DATA/files/LOGS/$LOG_FILENAME?overwrite=true" -X POST -F "file=@$LOG_FILE" -i | tee -a $LOG_FILE

	if ! $KEEP_TEMP_DIR && [[ $TMPDIR =~ ^.*LsLinked$ ]] ; then
		printf "\nCleaning up temp directory:  $TMPDIR\n" | tee -a $LOG_FILE
		rm -r $TMPDIR
	fi

fi

