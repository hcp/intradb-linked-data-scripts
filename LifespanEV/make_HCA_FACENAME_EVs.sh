#!/bin/bash

inputFile=$1;			#`ls ${runfolder}/*${task}*wide.csv`
outputDirectory=$2;		#${runfolder}/EVs

# Use this bash file to generate EVs for the CARIT task
task="FACENAME";
df="10"; # number of frames dropped in dicom2nifti = 10
tr="0.8";
timeseriesOnset=`echo "${tr}*${df}" | bc`;
trialsPerBlock=5;

mkdir -p ${outputDirectory}
echo -n '' >| ${outputDirectory}/encoding.txt
echo -n '' >| ${outputDirectory}/recall.txt

# for the commands below, tr breaks the columns into separate lines
# grep -n returns the line number for the matching column
# then awk prints the line number (which is also the column number)
columns=$( head -1 $inputFile );
N=`echo $columns | tr "," "\n" | grep -nw trialNum | awk -F : '{print $1}'`;
B=`echo $columns | tr "," "\n" | grep -nw block | awk -F : '{print $1}'`;
S=`echo $columns | tr "," "\n" | grep -nw faceStartTime | awk -F : '{print $1}'`;
E=`echo $columns | tr "," "\n" | grep -nw faceEndTime | awk -F : '{print $1}'`;


# Simplify $inputFile to give only relevant events, and only columns needed for EVs
#awk '{ FS =  "," } ; {print $"'$B'"}' ${inputFile} | grep "MEMORIZE\|RECALL" | sed -e "s/MEMORIZE/ENCODING/g" >| block
awk '{ FS =  "," } ; {print $"'${N}'",$"'${B}'",$"'${S}'",$"'${E}'"}' ${inputFile} | grep "MEMORIZE\|RECALL" | sed -e "s/MEMORIZE/ENCODING/g" >| tmpTrials

echo -n '' >| tmpBlock;
block=0;
for trial in $( awk '{print $1}' tmpTrials ); do
	if [ "$( expr $trial % 5 )" -eq "1" ]; then
		let "block += 1";
	fi;
	echo "$block" >> tmpBlock;
done


# Make each EV text file
paste tmpBlock tmpTrials >| trials
for block in `awk '{print $1}' trials | uniq`; do
	outfile=`grep "^${block}" trials | awk '{print $3}' | uniq |  tr '[:upper:]' '[:lower:]'`;
	echo $block $outfile;
	S=`grep "^${block}" trials | awk 'NR==1{print $(NF-1)}'`
	NS=`echo "${S}-$timeseriesOnset" | bc`;
	F=`grep "^${block}" trials | awk 'END{print $NF}'`
	D=`echo "${F}-${S}" | bc`;
	echo "${NS} ${D} 1" >> ${outputDirectory}/${outfile}.txt
done


# Remove the simplified temp files
rm -f tmpTrials tmpBlock trials


