#!/bin/bash

inputFile=$1;			#`ls ${runfolder}/*${task}*wide.csv`
outputDirectory=$2;		#${runfolder}/EVs

# Use this bash file to generate EVs for the CARIT task
task="CARIT";
df="10"; # number of frames dropped in dicom2nifti = 10
tr="0.8";
timeseriesOnset=`echo "${tr}*${df}" | bc`;


mkdir -p ${outputDirectory}
touch ${outputDirectory}/nogoFA.txt ${outputDirectory}/nogoCR.txt ${outputDirectory}/go.txt ${outputDirectory}/miss.txt

# for the commands below, tr breaks the columns into separate lines
# grep -n returns the line number for the matching column
# then awk prints the line number (which is also the column number)
columns=$( head -1 $inputFile );
# R gives field for Correct Response Message [ Hit, corReject, falseAlarm, Miss ]
R=`echo $columns | tr "," "\n" | grep -nw corrRespTrialType | awk -F : '{print $1}'`;
# S gives field for Start Time for EV
S=`echo $columns | tr "," "\n" | grep -nw shapeStartTime | awk -F : '{print $1}'`;

# Simplify $inputFile to give only relevant events, and only columns needed for EVs
sed -e "s/\,\ //g" ${inputFile} | awk '{ FS =  "," } ; {print $"'$R'",$"'$S'"}' >| ${outputDirectory}/carit_events

# Make each EV text file
grep falseAlarm ${outputDirectory}/carit_events | awk '{print $NF-"'${timeseriesOnset}'",0.6,1}' >| ${outputDirectory}/nogoFA.txt
grep corReject ${outputDirectory}/carit_events | awk '{print $NF-"'${timeseriesOnset}'",0.6,1}' >| ${outputDirectory}/nogoCR.txt
grep Hit ${outputDirectory}/carit_events | awk '{print $NF-"'${timeseriesOnset}'",0.6,1}' >| ${outputDirectory}/go.txt
grep Miss ${outputDirectory}/carit_events | awk '{print $NF-"'${timeseriesOnset}'",0.6,1}' >| ${outputDirectory}/miss.txt

# Remove the simplified temp file
rm -f ${outputDirectory}/carit_events 


