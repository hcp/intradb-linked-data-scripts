# HCD
## REST
** just need to copy original files to new location without timestamp **
``` bash
for file in *design* *ts* *EyeCamFPS_Dist* *mp4*; do
	copy_psychopy_remove_filename_timestamps.sh $file;
done
```

## mbPCASL
** just need to copy original files to new location without timestamp **
``` bash
for file in *design* *ts* *EyeCamFPS_Dist* *mp4*; do
	copy_psychopy_remove_filename_timestamps.sh $file;
done
```

## CARIT
``` bash
column_string="trialNum stim corrAns prepotency ISI countdownStartTime countdownLabel shapeStartTime shapeEndTime fixStartTime fixEndTime trialOutcome nogoCondition resp trialResp.keys trialResp.rt trialResp.firstKey trialResp.firstRt corrRespMsg corrRespCode corrRespTrialType isiPress.keys isiPress.rt hitCount missCount falseAlarmCount corrRejectCount totalAcc goAcc nogoAcc goFiveBackAvg nogoFiveBackAvg totalFiveBackAvg prevRewShape condFile mriMode frameRate git-revision nRuns"

# These inputs are the original files with timestamps in filenames
# Output files written in $PWD without timestamps
#for file in $(ls *run{1,2}*wide.csv); do
	python make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
#done

./make_HCD_CARIT_EVs.sh $cleanInputFile $evOutputDirectory

```

## GUESSING
``` bash
column_string="trialNum countdownStartTime countdownLabel cueStartTime cueEndTime isi1StartTime isi1EndTime ISI1 guessStartTime guessEndTime isi2StartTime isi2EndTime ISI2 feedbackStartTime feedbackEndTime feedbackName valueCondition guessResp.firstKey guessResp.firstRt guessResp.firstGuess guessResp.keys guessResp.rt isiPress1.keys isiPress1.rt isiPress2.keys isiPress2.rt cumulativeNoResp cumulativeReward rewardAmount rewShape mriMode nRuns frameRate git-revision run"

# These inputs are the original files with timestamps in filenames
# Output files written in $PWD without timestamps
for file in $(ls *run{1,2}*wide.csv); do
	python make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
done

./make_HCD_GUESSING_EVs.sh $cleanInputFile $evOutputDirectory

```

## EMOTION
``` bash
column_string="trialNum trialCondition countdownStartTime countdownLabel cueStartTime cueEndTime trialStartTime trialEndTime fixStartTime fixEndTime runEndTime stimLeft stimTop stimRight corrAns trialResp.keys trialResp.rt resp msg corrRespMsg corrRespCode countConsecutive mriMode frameRate counterbalance"

# These inputs are the original files with timestamps in filenames
# Output files written in $PWD without timestamps
for file in $(ls *run{1,2}*wide.csv); do
	python make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
done

./make_HCD_EMOTION_EVs.sh $cleanInputFile $evOutputDirectory

```



# HCA
## REST
** just need to copy original files to new location without timestamp **
``` bash
for file in *design* *ts* *EyeCamFPS_Dist* *mp4*; do
	copy_psychopy_remove_filename_timestamps.sh $file;
done
```

## mbPCASL
** just need to copy original files to new location without timestamp **
``` bash
for file in *design* *ts* *EyeCamFPS_Dist* *mp4*; do
	copy_psychopy_remove_filename_timestamps.sh $file;
done
```

## CARIT
``` bash
column_string="trialNum stim corrAns prepotency ISI countdownStartTime countdownLabel shapeStartTime shapeEndTime fixStartTime fixEndTime runEndTime resp trialResp.keys trialResp.rt trialResp.firstKey trialResp.firstRt corrRespMsg corrRespCode corrRespTrialType isiPress.keys isiPress.rt hitCount missCount falseAlarmCount corrRejectCount totalAcc goAcc nogoAcc goFiveBackAvg nogoFiveBackAvg totalFiveBackAvg condFile mriMode frameRate  git-revision nRuns"

# These inputs are the original files with timestamps in filenames
# Output files written in $PWD without timestamps
for file in $(ls *run{1,2}*wide.csv); do
	python make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
done

./make_HCA_CARIT_EVs.sh $cleanInputFile $evOutputDirectory

```

## VISMOTOR
``` bash
column_string="trialNum countdownStartTime countdownLabel fixStartTime fixEndTime fixDur blockStartTime blockEndTime targetStartTime targetEndTime side firstKey firstRt correct trialResp.keys trialResp.rt earlyISI.keys lateISI.keys feedback resp respLast noResp correctResponse leftCount rightCount leftCorrCount rightCorrCount leftAcc rightAcc totalAcc totalFiveBackAvg countConsecutive trialsList mriMode runNumber frameRate git-revision run"

# These inputs are the original files with timestamps in filenames
# Output files written in $PWD without timestamps
for file in $(ls *run{1,2}*wide.csv); do
	python make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
done

./make_HCA_VISMOTOR_EVs.sh $cleanInputFile $evOutputDirectory

```

## FACENAME
``` bash
column_string="trialNum block stim FaceLabel countdownStartTime countdownLabel cueStartTime cueEndTime faceStartTime faceEndTime gngShapeStartTime gngShapeEndTime fixStartTime fixEndTime runEndTime resp trialResp.keys trialResp.rt corrAns ISI respGNG.keys respGNG.corr respGNG.rt isiPress.keys isiPress.rt runNum mriMode frameRate counterbalance git-revision"

# These inputs are the original files with timestamps in filenames
# Output files written in $PWD without timestamps
for file in $(ls *run{1,2}*wide.csv); do
	python make_public_psychopy_logfile.py --logfile $file --choose-cols $column_string --suffix date
done

./make_HCA_FACENAME_EVs.sh $cleanInputFile $evOutputDirectory

```
