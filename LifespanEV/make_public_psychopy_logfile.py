#!/usr/bin/env python
# coding: utf-8
# Update confusing values,
# and remove datestamp from logfiles
# EKK 2017-06
import argparse
import os
import pandas as pd
import re
import sys


def main(logfile, choose_cols=[], updates=[], suffix='date', outfile=None):
    '''Adjust a logfile to select specified columns or change column values.

    1) Specify columns to keep: `--choose-cols trialNum firstKey totalAcc`
    2) Update the values of rows in specific columns (e.g. “prevRewNogo” -> “Nogo”)
	3) Update the filename to strip out the date stamp
    
    Expects stdin if no logfile is passed, writes to stdout if filename is '-'.

    Output filename options also include an optional suffix (instead of stripping
    the date), and a full output name or '-' for stdout are also accepted.

    Row updates should be a list of tuples of (column, old_value, new_value)
    used to update specific values.
    For example, to fix Nogo condition in CARIT that had no paired
    reward associaiton GUESSING task:

        [('nogocondition', 'prevRewNogo', 'Nogo'),
         ('nogocondition', 'neutralNogo', 'Nogo')]

    The new_value is currently static, but could easily adjusted to take an
    expression (e.g. if we wanted to do column subtraction, etc.)

    Example:
    #########

    python ./make_public_psychopy_logfile.py \
        --logfile CARIT_HCP99999_run1_2017-06-03_161036_wide.csv \
        --choose-cols trialNum firstKey totalAcc \
        --updates nogoCondition prevRewNogo Nogo \
        --updates nogoCondition neutralNogo Nogo \
        --suffix date
    '''

    # Load Logfile
    tmpdf = pd.read_csv(logfile)
    
    # Check that all requested columns are present in DataFrame
    for column in choose_cols:
       if(column not in tmpdf.columns):
          if column == 'prevRewShape' and 'HCD' in logfile.name:
             # prevRewShape is missing in early HCD CARIT files!!
             # Insert column containing circle.png in missing files
             print column, "is missing in HCD logfile. Column for circle.png condition being added."
             tmpdf[column] = "circle.png"
          else:
             # for example, ISI.rt column (RTs for button presses during ISI) will be 
             # missing if no responses are made in ISI during task.
             print column, "is missing. Adding blank column to file."
             tmpdf[column] = "[]"

	# Select specific columns
    df = tmpdf[choose_cols]

    # Update incorrect values
    for (column, old_value, new_value) in updates:
        df.loc[df[column] == old_value, column] = new_value

    # Determine output filename
    out = set_outfile(logfile, outfile, suffix)

    # Save logfile
    df.to_csv(out, index=False)


def set_outfile(logfile, outfile, suffix):
    if not outfile:
        if 'name' in dir(logfile):  # Not opened with stdin
            base, ext = os.path.splitext(os.path.basename(logfile.name))
            if suffix == 'date':  # Specifical case: Remove date from fname
                out = remove_date(base) + ext
            else:
                out = base + suffix + ext
            print out
        else:
            msg = ('Please provide either an outfile name ("-" for '
                   'stdout) or an input file name and suffix')
            raise StandardError(msg)
    elif outfile == '-':
        out = sys.stdout
    return out


def remove_date(fname, date_pat='(\d{4}\-\d{2}\-\d{2}_\d{6}_)'):
    match = re.search(date_pat, fname)
    if match:
        fname = fname.replace(match.groups()[0], '')
    else:
        msg = ('Requested date to be removed, but the pattern %s found no date'
               % date_pat)
        raise StandardError(msg)
    return fname


def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('--logfile',
                        '-l',
                        type=argparse.FileType('r'),
                        default=sys.stdin,
                        help='Logfile to parse')

    parser.add_argument('--choose-cols',
                        '-c',
                        nargs='*',
                        help=('Choose only these column names from logfile ' +
                              '(eg `--choose-cols session Gender`'))

    parser.add_argument('--outfile',
                        '-o',
                        type=argparse.FileType('w'),
                        default=None,
                        help='Output log')

    parser.add_argument('--suffix',
                        '-s',
                        type=str,
                        default='date',
                        help=('String to insert to cleaned filename, or ' +
                              '"date" to remove datestamp'))

    parser.add_argument('--updates',
                        '-u',
                        type=str,
                        default=[],
                        nargs=3,
                        action='append',
                        help='Update values: column-name old-value new-value')

    return parser.parse_args(args)


if __name__ == '__main__':
    args = parse_args(sys.argv[1:])
    main(**vars(args))
