#!/bin/bash

inputFile=$1;			#`ls ${runfolder}/*${task}*wide.csv`
outputDirectory=$2;		#${runfolder}/EVs

# Use this bash file to generate EVs for the GUESSING task
task="GUESSING";
df="10"; # number of frames dropped in dicom2nifti = 10
tr="0.8";
timeseriesOnset=`echo "${tr}*${df}" | bc`;


#> • GUESSING
#>  • Set filenames (case sensitive): guess [done] cueHigh [done] cueLow [done] feedbackHighLose feedbackHighWin feedbackLowLose feedbackLowWin
#>  • Remove EVs other than those above

mkdir -p ${outputDirectory}

# for the commands below, tr breaks the columns into separate lines
# grep -n returns the line number for the matching column
# then awk prints the line number (which is also the column number)
columns=$( head -1 $inputFile );
S=`echo $columns | tr "," "\n" | grep -nw guessStartTime | awk -F : '{print $1}'`;
G=`echo $columns | tr "," "\n" | grep -nw 'guessResp.firstKey' | awk -F : '{print $1}'`;
F=`echo $columns | tr "," "\n" | grep -nw feedbackStartTime | awk -F : '{print $1}'`;
N=`echo $columns | tr "," "\n" | grep -nw feedbackName | awk -F : '{print $1}'`;
V=`echo $columns | tr "," "\n" | grep -nw valueCondition | awk -F : '{print $1}'`;
C=`echo $columns | tr "," "\n" | grep -nw cueStartTime | awk -F : '{print $1}'`;

# Simplify $inputFile to give only relevant events, and only columns needed for EVs
# some rows in $inputFile have blank or weird values (because they're not guess or cue events)
# use grep -v to ignore them, and use sed to replace missing response with 'X'
awk '{ FS =  "," } ; {print $"'$G'",$"'$V'",$"'$N'",$"'$S'",$"'$F'"}' ${inputFile}  | grep -v " $" | sed -e "s/^ /X /g" >| guess_events
awk '{ FS =  "," } ; {print $"'$C'",$"'$V'",$"'$N'"}' ${inputFile} | grep -v "^ " >| cue_events

# Assumes that all cues are 1.5s in length
for stake in high low; do
	stakeFormattedString=$( echo $stake | sed -e 's|high|High|' -e 's|low|Low|' );
	# column $1 corresponds to $C (cueStartTime)
	awk '/'${stake}'/{print $1-'"${timeseriesOnset}"',1.5,1}' cue_events >| ${outputDirectory}/cue${stakeFormattedString}.txt
done

# column $4 corresponds to $S (guessStartTime)
# Assumes that all guess events are 2.0s in length
awk '{print $4-'"${timeseriesOnset}"',2,1}' < guess_events >| ${outputDirectory}/guess.txt

# column $NF (last column) corresponds to $F (feedbackStartTime)
grep Win guess_events | grep high | awk '{print $NF-'"${timeseriesOnset}"',2,1}' >| ${outputDirectory}/feedbackHighWin.txt
grep Lose guess_events | grep high | awk '{print $NF-'"${timeseriesOnset}"',2,1}' >| ${outputDirectory}/feedbackHighLose.txt
grep Win guess_events | grep low | awk '{print $NF-'"${timeseriesOnset}"',2,1}' >| ${outputDirectory}/feedbackLowWin.txt
grep Lose guess_events | grep low | awk '{print $NF-'"${timeseriesOnset}"',2,1}' >| ${outputDirectory}/feedbackLowLose.txt

# Remove the simplified temp files
rm -f guess_events cue_events
