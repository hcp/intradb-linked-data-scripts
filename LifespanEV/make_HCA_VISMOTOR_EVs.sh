#!/bin/bash

inputFile=$1;			#`ls ${runfolder}/*${task}*wide.csv`
outputDirectory=$2;		#${runfolder}/EVs

# Use this bash file to generate EVs for the CARIT task
task="VISMOTOR";
df="10"; # number of frames dropped in dicom2nifti = 10
tr="0.8";
timeseriesOnset=`echo "${tr}*${df}" | bc`;


mkdir -p ${outputDirectory}
echo -n '' >| ${outputDirectory}/vismotor.txt

# for the commands below, tr breaks the columns into separate lines
# grep -n returns the line number for the matching column
# then awk prints the line number (which is also the column number)
columns=$( head -1 $inputFile );
T=`echo $columns | tr "," "\n" | grep -nw blockStartTime | awk -F : '{print $1}'`;
E=`echo $columns | tr "," "\n" | grep -nw blockEndTime | awk -F : '{print $1}'`;
B=`echo $columns | tr "," "\n" | grep -nw trialsList | awk -F : '{print $1}'`;
U=`awk '{ FS =  "," } ; {print $"'$B'"}' ${inputFile} | grep -v List | uniq`

# Make the EV text file
for block in `echo ${U}`; do
	S=`grep ${block} ${inputFile} | awk -F , 'NR==1{print $"'$T'"}'`;
	NS=`echo "${S}-${timeseriesOnset}" | bc`;
	F=`grep ${block} ${inputFile} | awk -F , 'END{print $'"$E"'}'`;
	D=`echo "${F}-${S}" | bc`;
	echo "${NS} ${D} 1" >> ${outputDirectory}/vismotor.txt;
done
