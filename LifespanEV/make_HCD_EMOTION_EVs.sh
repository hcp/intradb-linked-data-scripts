#!/bin/bash

inputFile=$1;			#`ls ${runfolder}/*${task}*wide.csv`
outputDirectory=$2;		#${runfolder}/EVs

# Use this bash file to generate EVs for the EMOTION task
task="EMOTION";
df="10"; # dropped frames = 10
tr="0.8";
timeseriesOnset=`echo "${tr}*${df}" | bc`;

# There is an assumption that all trials are ~16s in length
#rm -f block trials tmp
mkdir -p ${outputDirectory}
echo -n "" >| ${outputDirectory}/faces.txt
echo -n "" >| ${outputDirectory}/shapes.txt

# for the commands below, tr breaks the columns into separate lines
# grep -n returns the line number for the matching column
# then awk prints the line number (which is also the column number)
columns=$( head -1 $inputFile );
B=`echo $columns | tr "," "\n" | grep -n trialCondition | awk -F : '{print $1}'`; #block type
E=`echo $columns | tr "," "\n" | grep -n fixEndTime | awk -F : '{print $1}'`; #
CS=`echo $columns | tr "," "\n" | grep -n cueStartTime | awk -F : '{print $1}'`;

# Simplify $inputFile to give only relevant events, and only columns needed for EVs
# some rows in $inputFile have blank or weird values (because they're not guess or cue events)
# use grep -v to ignore them, and use sed to replace missing response with 'X'
awk '{ FS =  "," } ; {print $"'$B'",$"'$CS'",$"'$E'"}' ${inputFile} | grep "shape\|face" | awk '{print $1,$2}' > trials

events=(`awk '{print $1}' trials`); # $events is an array of all event types
numevents=`echo "${#events[*]} - 1" | bc`; # index of the last event in array

blockNumber=1;
echo ${blockNumber} >| block; # the first event is obviously part of the first block
# Compare each event to the prior event to number the blocks
# ${events[1]} is the second event
for i in `seq ${numevents}`; do
	# if this event is the same as the previous event
	if [ "${events[$i]}" == "${events[$i-1]}" ]; then
		# write the same block number
		echo ${blockNumber} >> block;
	else
		# write a new block number
		blockNumber=$((blockNumber+1));
		echo ${blockNumber} >> block;
	fi
done

paste block trials > tmp
# for each unique block number
for i in `awk '{print $1}' tmp | uniq`; do
	# S is the start of the block
	S=`grep "^${i}" tmp | awk 'NR==1{print $NF}'`
	# NS corrects start time for timeseriesOnset
  	NS=`echo "${S}-${timeseriesOnset}" | bc`;
	# F is the end of the block
  	F=`grep "^${i}" tmp | awk 'END{print $NF}'`
	# D is the block duration (end minus start)
	D=`echo "${F}-${S}" | bc`;
	# is this block a face(s) or shape(s) block?
	outfile=`grep "^${i}" tmp | awk '{print $2}' | uniq`;
	# write "start duration 1" for this block type
	echo "${NS} ${D} 1" >> ${outputDirectory}/${outfile}s.txt;
done

rm -f block trials tmp

