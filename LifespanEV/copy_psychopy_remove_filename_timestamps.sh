#!/bin/bash

infile=$1;

# perl replaces "_yyyy-mm-dd_hhmmss" with nothing and ensures underscore in front of 'EyeCamFPS'
outfile=$( echo $( basename $infile ) | perl -ne 's/_\d{4}-\d{2}-\d{2}_\d{6}//; s/([^_])EyeCamFPS/$1_EyeCamFPS/; print;' )

cp -v $infile $outfile
